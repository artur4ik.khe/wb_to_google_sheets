class Warehouse:

    WAREHOUSES = [
        "Электросталь",
        "Казань",
        "Санкт-Петербург",
        "Санкт-Петербург 2",
        "Краснодар 2",
        "Алексин",
        "МЛП-Подольск",
        "Коледино",
        "Новосибирск",
        "Екатеринбург",
        "Новосёлки Чехов",
        "СЦ Белая дача",
        "Хабаровск",
        "Нур-Султан",
    ]

    def __init__(self, name, quantity):
        self.name = name
        self.quantity = quantity


class Item:

    def __init__(
            self,
            nm_id: int,
            price: float,
            name: str,
            category: str,
            barcode: str,
            warehouse: str,
            quantity: str
    ):
        self.nm_id = nm_id
        self.price = price
        self.name = name
        self.category = category
        self.barcode = barcode
        self.warehouses_data = [Warehouse(warehouse, quantity)]

    def __eq__(self, other):
        return self.nm_id == other.nm_id

    def add_warehouse_data(self, name, quantity):
        self.warehouses_data.append(Warehouse(name, quantity))

    def _get_sum_quantity(self):
        result = [warehouse.quantity for warehouse in self.warehouses_data]
        return sum(result)

    def get_data_about_item(self):
        result = [self.name, self.barcode, self.category, self.nm_id, self.price, self._get_sum_quantity()]
        quantities = [0 for i in range(len(Warehouse.WAREHOUSES))]
        for item in self.warehouses_data:
            idx = Warehouse.WAREHOUSES.index(item.name)
            quantities[idx] = item.quantity
        result.extend(quantities)
        return result




