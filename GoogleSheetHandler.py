import pickle

import os
from google.auth.transport.requests import Request
from google.oauth2.credentials import Credentials
from google_auth_oauthlib.flow import InstalledAppFlow
from googleapiclient.discovery import build
from googleapiclient.errors import HttpError


class GoogleSheetHandler:
    SPREADSHEET_ID = '1LJkVlDwOlr814xDxh8YL7tH0pxxbtdKVr04rLg_G8NU'
    SCOPES = ['https://www.googleapis.com/auth/spreadsheets']
    service = None

    def __init__(self):
        creds = None
        if os.path.exists('token.pickle'):
            with open('token.pickle', 'rb') as token:
                creds = pickle.load(token)

        if not creds or not creds.valid:
            if creds and creds.expired and creds.refresh_token:
                creds.refresh(Request())
            else:
                print('flow')
                flow = InstalledAppFlow.from_client_secrets_file(
                    'credentials.json', self.SCOPES)
                creds = flow.run_local_server(port=0)
            with open('token.pickle', 'wb') as token:
                pickle.dump(creds, token)

        self.service = build('sheets', 'v4', credentials=creds)

    def updateRangeValues(self, sheet_range, values, table_id):
        data = [{
            'range': sheet_range,
            'values': values
        }]
        body = {
            'valueInputOption': 'USER_ENTERED',
            'data': data
        }
        result = self.service.spreadsheets().values().batchUpdate(spreadsheetId=table_id, body=body).execute()
        print('{0} cells updated.'.format(result.get('totalUpdatedCells')))

    def getRangeValues(self, sheet_range):
        return self.service.spreadsheets().values().get(
            spreadsheetId=self.SPREADSHEET_ID,
            range=sheet_range
        ).execute()["values"]

    def clear_range(self, sheet_range, table_id):
        data = [{
            'range': sheet_range,
            'values': []
        }]
        body = {
            'valueInputOption': 'USER_ENTERED',
            'data': data
        }
        result = self.service.spreadsheets().values().batchUpdate(spreadsheetId=table_id, body=body).execute()
        print('{0} cells updated.'.format(result.get('totalUpdatedCells')))
