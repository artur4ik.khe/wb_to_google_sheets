from __future__ import print_function
from time import sleep
import requests
import os.path
from dotenv import load_dotenv
from GoogleSheetHandler import GoogleSheetHandler
from Item import Item, Warehouse
from datetime import date

from Report import Report

load_dotenv()


WB_STOCKS_URL = f"https://statistics-api.wildberries.ru/api/v1/supplier/stocks?dateFrom={date.today()}"
WB_REPORT_URL = f"https://statistics-api.wildberries.ru/api/v1/supplier/reportDetailByPeriod"
token = os.getenv("WB_TOKEN")
SPREADSHEET_STOCKS =os.getenv("SPREADSHEET_STOCKS")
SPREADSHEET_REPORT =os.getenv("SPREADSHEET_REPORT")
gs = GoogleSheetHandler()


def get_api_data(url, params=None):
    try:
        r = requests.get(url, params=params, headers={'Authorization': token})
    except requests.exceptions.ChunkedEncodingError as e:
        r = re_request(url, params)
        return r
    while r.status_code != 200:
        sleep(60)
        print("sleep 60 sec, too many requests(")
        try:
            r = requests.get(url, params=params, headers={'Authorization': token})
        except requests.exceptions.ChunkedEncodingError as e:
            r = re_request(url, params)
            return r
        print(r.status_code)
    result = r.json()
    return result


def re_request(url, params):
    result = True
    while result:
        try:
            sleep(30)
            print("sleep chunked problems")
            response = requests.get(url, params=params, headers={'Authorization': token})
            result = False
            return response.json()
        except requests.exceptions.ChunkedEncodingError as e:
            print("truble", e)


def get_range_cells(sheets, begin_column, end_column, begin_row, end_row):
    return f"{sheets}!{begin_column}{begin_row}:{end_column}{end_row}"


def get_report_data():
    date_from = input("Input date from like this <2023-01-01>: ")
    date_to = input("Input date from like this <2023-01-01>: ")
    limit = 20000 #int(input("Limit: "))
    begin_row = 2
    end_row = limit + 1
    rrd_id = 0
    params = {
        "dateFrom": date_from,
        "limit": limit,
        "dateTo": date_to,
        "rrdid": rrd_id
    }
    try:
        reports = get_api_data(WB_REPORT_URL, params=params)
    except requests.exceptions.ChunkedEncodingError as e:
        reports = re_request(WB_REPORT_URL, params)
    while reports is not None:

        result = []
        for item in reports:
            item["retail_price_with_discount"] = int(item["retail_price"]) - (int(item["retail_price"]) * int(item["sale_percent"]) * 0.01)
            if "bonus_type_name" not in item:
                item["bonus_type_name"] = None
            report = [
                item["realizationreport_id"],
                item["create_dt"],
                item["date_from"],
                item["date_to"],
                item["rrd_id"],
                item["gi_id"],
                item["subject_name"],
                item["brand_name"],
                item["nm_id"],
                item["sa_name"],
                item["barcode"],
                item["doc_type_name"],
                item["quantity"],
                item["retail_price"],
                item["retail_price_with_discount"],
                item["retail_amount"],
                item["commission_percent"],
                item["office_name"],
                item["supplier_oper_name"],
                item["order_dt"],
                item["sale_dt"],
                item["rr_dt"],
                item["shk_id"],
                item["delivery_amount"],
                item["return_amount"],
                item["delivery_rub"],
                item["rid"],
                item["ppvz_for_pay"],
                item["ppvz_reward"],
                item["acquiring_fee"],
                item["bonus_type_name"],
                item["penalty"],
                item["additional_payment"]
            ]
            result.append(report)

        report_range = get_range_cells("report", "A", "AG", begin_row, end_row)
        print("update cells")
        gs.updateRangeValues(report_range, result, SPREADSHEET_REPORT)
        if limit != len(result):
            end_row = end_row - limit + len(result)
            begin_row = end_row
        else:
            begin_row = end_row + 1
            end_row = begin_row + limit - 1
        params.update({"rrdid": reports[-1]["rrd_id"]})
        sleep(30)
        try:
            reports = get_api_data(WB_REPORT_URL, params=params)
        except requests.exceptions.ChunkedEncodingError as e:
            reports = re_request(WB_REPORT_URL, params)


def get_data_stocks_warehouse():
    stocks_data = get_api_data(WB_STOCKS_URL)
    items_ids = []
    items = []
    for stock in stocks_data:
        nmId = stock["nmId"]
        price = stock["Price"]
        name = stock["subject"]
        category = stock["category"]
        barcode = stock["barcode"]
        warehouse = stock["warehouseName"]
        quantity = stock["quantity"]
        if nmId in items_ids:
            id = items_ids.index(nmId)
            items[id].add_warehouse_data(warehouse, quantity)
        else:
            item = Item(
                nmId,
                price,
                name,
                category,
                barcode,
                warehouse,
                quantity)
            items.append(item)
            items_ids.append(item.nm_id)

    return [item.get_data_about_item() for item in items]


def get_name_columns(response_item):
        return [key for key in response_item.keys()]


def get_menu():
    return f"Меню:\n\t" \
           f"1. Загрузить данные остатков по складам\n\t" \
           f"2. Загрузить отчеты"


def main():

    while True:
        print(get_menu())
        select = int(input("Выберите пункт меню: "))
        if select == 1:
            data = get_data_stocks_warehouse()
            warehouse_range = "warehouse!H:W"
            gs.updateRangeValues(warehouse_range, [Warehouse.WAREHOUSES], SPREADSHEET_STOCKS)
            data_range = 'warehouse!B2:W501'
            gs.updateRangeValues(data_range, data, SPREADSHEET_STOCKS)
        elif select == 2:
            get_report_data()
        else:
            break


if __name__ == '__main__':
    main()
