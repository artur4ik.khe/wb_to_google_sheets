

class Report:
    COLUMNS = [
        ""
    ]

    def __init__(
            self,
            realizationreport_id,
            create_dt,
            date_from,
            date_to,
            rrd_id,
            gi_id,
            subject_name,
            brand_name,
            nm_id,
            sa_name,
            barcode,
            doc_type_name,
            quantity,
            retail_price,
            retail_price_with_discount,
            retail_amount,
            commission_percent,
            office_name,
            supplier_oper_name,
            order_dt,
            sale_dt,
            rr_dt,
            shk_id,
            delivery_amount,
            return_amount,
            delivery_rub,
            rid,
            ppvz_for_pay,
            ppvz_reward,
            acquiring_fee,
            bonus_type_name,
            penalty,
            additional_payment
    ):
        self.realizationreport_id = realizationreport_id
        self.create_dt = create_dt
        self.date_from = date_from
        self.date_to = date_to
        self.rrd_id = rrd_id
        self.gi_id = gi_id
        self.subject_name = subject_name
        self.brand_name = brand_name
        self.nm_id = nm_id
        self.sa_name = sa_name
        self.barcode = barcode
        self.doc_type_name = doc_type_name
        self.quantity = quantity
        self.retail_price = retail_price
        self.retail_price_with_discount = retail_price_with_discount
        self.retail_amount = retail_amount
        self.commission_percent = commission_percent
        self.office_name = office_name
        self.supplier_oper_name = supplier_oper_name
        self.order_dt = order_dt
        self.sale_dt = sale_dt
        self.rr_dt = rr_dt
        self.shk_id = shk_id
        self.delivery_amount = delivery_amount
        self.return_amount = return_amount
        self.delivery_rub = delivery_rub
        self.rid = rid
        self.ppvz_for_pay = ppvz_for_pay
        self.ppvz_reward = ppvz_reward
        self.acquiring_fee = acquiring_fee
        self.bonus_type_name = bonus_type_name
        self.penalty = penalty
        self.additional_payment = additional_payment

    def get_data(self):
        return [
            self.realizationreport_id,
            self.create_dt,
            self.date_from,
            self.date_to,
            self.rrd_id,
            self.gi_id,
            self.subject_name,
            self.brand_name,
            self.nm_id,
            self.sa_name,
            self.barcode,
            self.doc_type_name,
            self.quantity,
            self.retail_price,
            self.retail_price_with_discount,
            self.retail_amount,
            self.commission_percent,
            self.office_name,
            self.supplier_oper_name,
            self.order_dt,
            self.sale_dt,
            self.rr_dt,
            self.shk_id,
            self.delivery_amount,
            self.return_amount,
            self.delivery_rub,
            self.rid,
            self.ppvz_for_pay,
            self.ppvz_reward,
            self.acquiring_fee,
            self.bonus_type_name,
            self.penalty,
            self.additional_payment
        ]
